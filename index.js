// @flow
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/Screens';

registerScreens(); // this is where you register all of your app's screens

Navigation.startSingleScreenApp({
  screen: {
    screen : 'Main',
    title: 'MiniApp Simulator'
  }
});

// import { AppRegistry } from 'react-native';
// import Main from './src/Main';
// AppRegistry.registerComponent('MiniAppSimulator', () => Main);


