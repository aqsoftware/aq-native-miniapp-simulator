//
//  Globals.h
//  MiniAppSimulator
//
//  Created by Ryan Brozo on /0129/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Globals : NSObject

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic) NSInteger port;
@property (nonatomic, readonly) NSURL* _Nonnull webRoot;
@property (nonatomic, readonly) NSString* _Nonnull webRootPath;

@end
