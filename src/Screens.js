// @flow

import { Navigation } from 'react-native-navigation';

import Main from './Main';
import Preview from './Preview';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('Main', () => Main);
  Navigation.registerComponent('Preview', () => Preview);
}