// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Alert,
  Image,
  TouchableOpacity
} from 'react-native';
import { FunTypeView, Messages } from 'aq-react-native-miniapp-sdk';
import icon_nav_back from './img/icon_nav_back_white.png';

type Props = {
  navigator: any,
  url: string
}

type State = {
}

export default class Preview extends React.Component<Props, State> {

  static defaultProps = {};

  static navigatorStyle = {
    navBarHidden: true,
    statusBarHidden: true
  }

  funTypeView: FunTypeView;

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  _onBack() {
    this.props.navigator.dismissModal();
  }

  _funTypeViewDidLoad(){
    console.log('funTypeViewDidLoad');
  }
  
  _funTypeViewReady(){
    console.log('funTypeViewReady');
  }

  _onSetAppData(appData: Object) {
    console.log(`_onSetAppData = ${JSON.stringify(appData)}`);
  }

  _onJoin(id: ?string, joinOutputImageUrl: string, winCriteriaPassed: boolean, notificationItem: ?NotificationItem){
    console.log('_onJoin');
  }

  _onEnd(){
    console.log('_onEnd');
  }

  _onMessage(params: {message: string}) {
    // let props = { ...this.props };
    console.log(`_onMessage = ${JSON.stringify(params)}`);
    switch (params.message) {
      case Messages.MESSAGE_START:
        break;
      case Messages.MESSAGE_CREATE_BET:
        break;
      case Messages.MESSAGE_CLAIM_BET:
        break;
      case Messages.MESSAGE_PAY:
        break;
      case Messages.MESSAGE_SHOW_FRIENDS_SELECTOR_PROMISE:
        break;
      default:
        break;
    }
  }

  _onRequestSelector(selector: string, key: string, data: Object) {
    // let props = { ...this.props };
    // props.title = `Send B: to friends:`;
    // props.onResult = ((friends) => this._onInviteResult(friends, key)).bind(this);

    console.log(`_onRequestSelector = ${selector} ${key} ${JSON.stringify(data)}`);

    switch (selector) {
      case Messages.MESSAGE_SHOW_TITLE_INPUT:
        // this.setState({
        //   selectorMode: 'title'
        // });
        break;
      case Messages.MESSAGE_GET_FRIENDS:
        // this._onGetFriends();
        break;
      case Messages.MESSAGE_GET_BM_BALANCE:
        // this._onGetBMBalance();
        break;
      case Messages.MESSAGE_SHOW_GALLERY_IMAGE_SELECTOR:
        // this.setState({
        //   galleryImageSelectorData: {
        //     'key': key,
        //     'title': data.title
        //   },
        //   selectorMode: 'gallery'
        // });
        break;
      case Messages.MESSAGE_SHOW_WEB_IMAGE_SELECTOR:
        // this.setState({
        //   webImageSelectorData: {
        //     'key': key,
        //     'title': data.title,
        //     'data': data.imageUrls
        //   },
        //   selectorMode: 'webImage'
        // });
        break;
      case Messages.MESSAGE_SHOW_FRIENDS_SELECTOR:
        // this.setState({
        //   selectorMode: 'none'
        // });
        // props.color = props.engagement.funType.family.color;
        // props.title = props.engagement.funType.family.name;
        // props.image = props.engagement.imageBig;
        // this.props.navigator.showModal({
        //   screen: 'InviteScene',
        //   title: 'Select friends',
        //   passProps: props,
        //   navigatorStyle: {
        //     screenBackgroundColor: '#404040'
        //   },
        //   animationType: 'none'
        // });
        break;
      default:

    }
  }

  _onLoadProgress(progress: number) {
    console.log(`onLoadProgress = ${progress}`);
  }

  render() {
    let engagementId = 'Rg3x4AS5EeiCX26thpmiiw';
    let funType = { 
      id: 'hJJ44AS6EeiCX26thpmiiw',
      name: 'preview',
      type: 2,
      webUrl: this.props.url,
      packageFileUrl: null,
      packageFileHash: null
    };
    return (
      <View style={styles.container}>
        <FunTypeView
          ref={(component: any) => this.funTypeView = component}
          funType={funType}
          engagementId={engagementId}
          mode='preview'
          style={styles.wholepage}
          onLoad={this._funTypeViewDidLoad.bind(this)}
          onError={(error) => {
            Alert.alert('Coming Soon',
              "It's worth the wait :)",
              [
                { text: 'OK', onPress: () => this.props.navigator.pop({ animated: false }) },
              ]
            );
          }}
          onRequestSelector={(selector, key, data) => this._onRequestSelector(selector, key, data)}
          onSetAppData={this._onSetAppData.bind(this)}
          onReady={this._funTypeViewReady.bind(this)}
          onJoin={this._onJoin.bind(this)}
          onEnd={this._onEnd.bind(this)}
          onMessage={this._onMessage.bind(this)}
          onLoadProgress={this._onLoadProgress.bind(this)}
        />
        <TouchableOpacity
          onPress={this._onBack.bind(this)}
        >
          <Image 
            style={styles.backButton}
            source={icon_nav_back}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: '#fff',
  },
  wholepage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  backButton: {
    width: 44,
    height: 44
  }
});