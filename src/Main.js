// @flow
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  Alert,
  Dimensions
} from 'react-native';

type Props = {
  navigator: any
}

type State = {
  url: string
}

export default class Join extends React.Component<Props, State> {

  static defaultProps = {};

  static navigatorStyle = {
    drawUnderNavBar: false,
    navBarTranslucent: true,
    title: 'Simulator'
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      url: ''
    };
  }

  _isUrl(url: string): boolean {
    var pattern = new RegExp('^((http|https):\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(url);
  }

  _onPreview() {
    if (this._isUrl(this.state.url)) {
      this.props.navigator.showModal({
        screen: 'Preview',
        title: 'Preview',
        passProps: {
          url: this.state.url
        }
      });
    }
    else {
      Alert.alert('Uh-oh',
        'Please input a valid URL',
        [
          { text: 'OK', onPress: () => this.props.navigator.pop({ animated: false }) },
        ]
      );
    }
  }

  render() {
    let { height, width } = Dimensions.get('window');
    return (
      <View style={styles.container}>
        <Text>URL:</Text>
        <TextInput
          style={styles.urlTextInput}
          onChangeText={(text) => this.setState({ url: text })}
          multiline={true}
          autoCapitalize='none'
          value={this.state.url}
        />
        <Button
          onPress={this._onPreview.bind(this)}
          title='Preview'
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  urlTextInput: {
    height: 80,
    left: 0,
    right: 0,
    borderColor: 'gray', 
    borderWidth: 1,
    borderRadius: 5
  }
});